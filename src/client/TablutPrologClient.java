/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import alice.tuprolog.NoSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Theory;
import alice.tuprolog.UnknownVarException;
import domain.*;
import domain.State.Turn;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.UnknownHostException;


/**
 *
 * @author matte
 */
public class TablutPrologClient extends TablutClient {
	
	  public TablutPrologClient(String player, String ipAddress) throws UnknownHostException, IOException {
	        super(player, "kebiInterface", ipAddress);
	    }

	    public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {

	        if (args.length == 0) {
	            System.out.println("You must specify which player you are (WHITE or BLACK)!");
	            System.exit(-1);
	        }
	        System.out.println("Selected this: " + args[0]);

	        TablutClient client = new TablutPrologClient(args[0], args[2]);

	        client.run();

	    }
	
   public Action getAction(SolveInfo info) throws IOException, NoSolutionException, UnknownVarException {
		Action result;
		char[] from = new char[2];
		char[] to = new char[2];
		from[1] = info.getTerm("XFrom").toString().charAt(0);
		from[0] = (char) (info.getTerm("YFrom").toString().charAt(0) + 16);
		to[1] = info.getTerm("XTo").toString().charAt(0);
		to[0] = (char) (info.getTerm("YTo").toString().charAt(0) + 16);

		String actionStringFrom = new String(from).toUpperCase();
		String actionStringTo = new String(to).toUpperCase();

		result = new Action(actionStringFrom, actionStringTo, this.getPlayer());
		return result;
	}
	

    @Override
    public void run() {
        System.out.println("You are player " + this.getPlayer().toString() + "!");
        Action action;
        //BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            this.declareName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.getPlayer() == Turn.WHITE) {
            System.out.println("You are player " + this.getPlayer().toString() + "!");
            while (true) {
                try {
                    this.read();

                    System.out.println("Current state:");
                    System.out.println(this.getCurrentState().toString());

                    State s = this.getCurrentState();

                 
                    if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {
                        System.out.println("Player " + this.getPlayer().toString() + ", do your move: ");
                       
                        /*QUERY A PROLOG PER STABILIRE MOSSA*/
                        Prolog engine = new Prolog();
                        String board=s.boardStringTuProlog();
                        //System.out.println(board);
                        String query = "alphabeta("+board+", -500, 500, GoodPos, Val, white, 0), getAction("+board+",GoodPos, XFrom, YFrom, XTo, YTo, white).";
                        SolveInfo info;

                        Theory t = new Theory(new FileInputStream("./WhiteTuProlog.pl"));
                        engine.setTheory(t);
                        
                        System.out.println("Eseguo la query a prolog");
                        info = engine.solve(query);
       
                        
                        action=this.getAction(info);                       
                       this.write(action);
                        
                        
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {
                        System.out.println("Waiting for your opponent move... ");
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITEWIN)) {
                        System.out.println("YOU WIN!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACKWIN)) {
                        System.out.println("YOU LOSE!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.DRAW)) {
                        System.out.println("DRAW!");
                        System.exit(0);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        } else {
            System.out.println("You are player " + this.getPlayer().toString() + "!");
            while (true) {
                try {
                	this.read();

                    System.out.println("Current state:");
                    System.out.println(this.getCurrentState().toString());

                    State s = this.getCurrentState();

                    if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {
                        System.out.println("Player " + this.getPlayer().toString() + ", do your move: ");
                        System.out.println(s.boardStringTuProlog());
                        
                        /*QUERY A PROLOG PER STABILIRE MOSSA*/
                        Prolog engine = new Prolog();
                        String board=s.boardStringTuProlog();
                        String query = "alphabeta("+board+", -500, 500, GoodPos, Val, black, 0), getAction("+board+",GoodPos, XFrom, YFrom, XTo, YTo, black).";
                        SolveInfo info;

                        Theory t = new Theory(new FileInputStream("./BlackTuProlog.pl"));
                        engine.setTheory(t);
                        
                        System.out.println("Eseguo la query a prolog");
                        info = engine.solve(query);

                                        
                        action=this.getAction(info);                       
                        this.write(action);
                        
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {
                        System.out.println("Waiting for your opponent move... ");
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITEWIN)) {
                        System.out.println("YOU LOSE!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACKWIN)) {
                        System.out.println("YOU WIN!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.DRAW)) {
                        System.out.println("DRAW!");
                        System.exit(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }

    }
}
