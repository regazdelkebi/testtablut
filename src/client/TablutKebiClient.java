/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import aima.core.search.adversarial.AdversarialSearch;

import domain.*;
import domain.State.Turn;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import kebitablut.MyGameTablut;
import kebitablut.MyIterativeDeepeningAlphaBetaSearch;

/**
 *
 * @author matte
 */
public class TablutKebiClient extends TablutClient {

    int timeout;
    
    public TablutKebiClient(String player, int timeout, String ipAddress) throws UnknownHostException, IOException {
        super(player, "kebiInterface", ipAddress);
        this.timeout = timeout;
    }

    public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {

        if (args.length == 0) {
            System.out.println("You must specify which player you are (WHITE or BLACK)!");
            System.exit(-1);
        }
        System.out.println("Selected this: " + args[0]);

        TablutClient client = new TablutKebiClient(args[0], Integer.parseInt(args[1]), args[2]);

        client.run();

    }

    @Override
    public void run() {
        System.out.println("You are player " + this.getPlayer().toString() + "!");
        Action action;

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            this.declareName();
        } catch (Exception e) {
        }

        MyGameTablut g = new MyGameTablut();
        StateTablut s;

        AdversarialSearch<StateTablut, Action> myAlphabeta = new MyIterativeDeepeningAlphaBetaSearch(g, 10, 180, timeout-5);

        if (this.getPlayer() == Turn.WHITE) {
            System.out.println("You are player " + this.getPlayer().toString() + "!");
            while (true) {
                try {
                    this.read();

//                    System.out.println("Current state:");
//                    System.out.println(this.getCurrentState().toString());

                    s = (StateTablut) this.getCurrentState();

                    if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {
//                        System.out.println("Player " + this.getPlayer().toString() + ", do your move: ");

                        g.actualState = s;
                        
                        action = (Action) myAlphabeta.makeDecision(s);
                        
                        this.write(action);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {
                        System.out.println("Waiting for your opponent move... ");
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITEWIN)) {
                        System.out.println("YOU WIN!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACKWIN)) {
                        System.out.println("YOU LOSE!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.DRAW)) {
                        System.out.println("DRAW!");
                        System.exit(0);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        } else {
            System.out.println("You are player " + this.getPlayer().toString() + "!");
            while (true) {
                try {
                    this.read();

//                    System.out.println("Current state:");
                    System.out.println(this.getCurrentState().toString());

                    s = (StateTablut) this.getCurrentState();

                    if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {
//                        System.out.println("Player " + this.getPlayer().toString() + ", do your move: ");

                        g.actualState = s;
                        
                        action = (Action) myAlphabeta.makeDecision(s);

                        this.write(action);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {
                        System.out.println("Waiting for your opponent move... ");
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITEWIN)) {
                        System.out.println("YOU WIN!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACKWIN)) {
                        System.out.println("YOU LOSE!");
                        System.exit(0);
                    } else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.DRAW)) {
                        System.out.println("DRAW!");
                        System.exit(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }

    }
}

