package client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;
import java.net.UnknownHostException;

import org.jpl7.*;

import domain.Action;
import domain.State;
import domain.StateTablut;
import domain.State.Turn;

public class SwiPrologClient extends TablutClient {

	public SwiPrologClient(String player, String ipAddress) throws UnknownHostException, IOException {
		super(player, "SwiKebiInterface", ipAddress);
	}

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {

		if (args.length == 0) {
			System.out.println("You must specify which player you are (WHITE or BLACK)!");
			System.exit(-1);
		}
		System.out.println("Selected this: " + args[0]);

		TablutClient client = new SwiPrologClient(args[0], args[2]);

		client.run();

	}

	public void createTheoryFile(State s, String path, String[] files, String outFile) throws IOException {
		int i, j;

		File boardFile = new File(path);
		FileWriter fw = new FileWriter(boardFile);

		for (i = 0; i < s.getBoard().length; i++) {
			for (j = 0; j < s.getBoard()[i].length; j++) {
				switch (s.getBoard()[i][j].toString()) {
				case ("W"):
					fw.write("initial(piece(white,pawn," + (i + 1) + "," + (j + 1) + ")).\n");
					break;
				case ("B"):
					fw.write("initial(piece(black,pawn," + (i + 1) + "," + (j + 1) + ")).\n");
					break;
				case ("K"):
					fw.write("initial(piece(white,king," + (i + 1) + "," + (j + 1) + ")).\n");
					break;
				default:
					break;
				}
			}
		}

		fw.flush();
		fw.close();

		OutputStream tmpOut = new FileOutputStream(outFile);
		byte[] buf = new byte[64];

		for (String file : files) {
			InputStream tmpIn = new FileInputStream(file);
			int b = 0;
			while ((b = tmpIn.read(buf)) >= 0) {
				tmpOut.write(buf, 0, b);
			}
			tmpIn.close();
		}
		tmpOut.close();
	}

	public Action getAction(Query query) throws IOException {
		Action result;
		char[] from = new char[2];
		char[] to = new char[2];
		java.util.Map<String, Term> solution;
		solution = query.oneSolution();
		from[1] = solution.get("XFrom").toString().charAt(0);
		from[0] = (char) (solution.get("YFrom").toString().charAt(0) + 16);
		to[1] = solution.get("XTo").toString().charAt(0);
		to[0] = (char) (solution.get("YTo").toString().charAt(0) + 16);

		String actionStringFrom = new String(from).toUpperCase();
		String actionStringTo = new String(to).toUpperCase();

		result = new Action(actionStringFrom, actionStringTo, this.getPlayer());
		return result;
	}

	@Override
	public void run() {

		System.out.println("You are player " + this.getPlayer().toString() + "!");
		Action action;

		try {
			this.declareName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (this.getPlayer() == Turn.WHITE) {
			System.out.println("You are player " + this.getPlayer().toString() + "!");
			while (true) {
				try {
					this.read();

					System.out.println("Current state:");
					System.out.println(this.getCurrentState().toString());

					State s = this.getCurrentState();

					/* SCRIVO IL FILE THEORY */
					/* INIZIALIZZO VARIABILI PER SCRIVERE */
					String path = "./Swi/Board.pl";
					String[] files = new String[2];

					files[0] = "./Swi/WhiteSwi.pl";
					files[1] = "./Swi/Board.pl";

					String outFile = "./Swi/TheoryWhiteSwi.pl";

					/* SCRIVO IL FILE THEORY */
					// PROBLEMA:-->SE FAI LE MOSSE E7->F7 e di seguito E6->F6 si pianta
					this.createTheoryFile(s, path, files, outFile);

					if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {
						System.out.println("Player " + this.getPlayer().toString() + ", do your move: ");

						/* QUERY A PROLOG PER STABILIRE MOSSA */

						/* INIZIALIZZO VARIABILI PER SCRIVERE */

						String queryString = "consult('" + outFile + "')";
						;
						Query query = new Query(queryString);

						if (query.hasSolution()) {

							Variable XFrom = new Variable("XFrom");
							Variable YFrom = new Variable("YFrom");
							Variable XTo = new Variable("XTo");
							Variable YTo = new Variable("YTo");

							query = new Query("getAction", new Term[] { XFrom, YFrom, XTo, YTo, new Atom("white") });

							if (query.hasSolution()) {

								action = this.getAction(query);
								this.write(action);
							}

						}

					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {
						System.out.println("Waiting for your opponent move... ");
					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITEWIN)) {
						System.out.println("YOU WIN!");
						System.exit(0);
					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACKWIN)) {
						System.out.println("YOU LOSE!");
						System.exit(0);
					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.DRAW)) {
						System.out.println("DRAW!");
						System.exit(0);
					}

				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		} else {
			System.out.println("You are player " + this.getPlayer().toString() + "!");
			while (true) {
				try {
					this.read();

					System.out.println("Current state:");
					System.out.println(this.getCurrentState().toString());

					State s = this.getCurrentState();

					/* SCRIVO IL FILE THEORY */
					/* INIZIALIZZO VARIABILI PER SCRIVERE */
					String path = "./Swi/Board.pl";
					String[] files = new String[2];

					files[0] = "./Swi/BlackSwi.pl";
					files[1] = "./Swi/Board.pl";

					String outFile = "./Swi/TheoryBlackSwi.pl";

					/* SCRIVO IL FILE THEORY */
					// PROBLEMA:-->SE FAI LE MOSSE E7->F7 e di seguito E6->F6 si pianta
					this.createTheoryFile(s, path, files, outFile);

					if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {
						System.out.println("Player " + this.getPlayer().toString() + ", do your move: ");

						/* QUERY A PROLOG PER STABILIRE MOSSA */

						/* INIZIALIZZO VARIABILI PER SCRIVERE */

						String queryString = "consult('" + outFile + "')";
					
						Query query = new Query(queryString);

						if (query.hasSolution()) {

							Variable XFrom = new Variable("XFrom");
							Variable YFrom = new Variable("YFrom");
							Variable XTo = new Variable("XTo");
							Variable YTo = new Variable("YTo");

							query = new Query("getAction", new Term[] { XFrom, YFrom, XTo, YTo, new Atom("black") });

							if (query.hasSolution()) {

								action = this.getAction(query);
								this.write(action);
							}

						}

					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {
						System.out.println("Waiting for your opponent move... ");
					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITEWIN)) {
						System.out.println("YOU LOSE!");
						System.exit(0);
					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACKWIN)) {
						System.out.println("YOU WIN!");
						System.exit(0);
					} else if (this.getCurrentState().getTurn().equals(StateTablut.Turn.DRAW)) {
						System.out.println("DRAW!");
						System.exit(0);
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}
}
