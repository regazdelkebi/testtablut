/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kebitablut;

import aima.core.search.adversarial.AdversarialSearch;
import aima.core.search.adversarial.AlphaBetaSearch;
import aima.core.search.adversarial.Game;
import aima.core.search.framework.Metrics;


/**
 *
 * @author matte
 */
public class MyAlphaBetaSearch<S, A, P> implements AdversarialSearch<S, A> {

    public final static String METRICS_NODES_EXPANDED = "nodesExpanded";
    public int maxDepth;
    Game<S, A, P> game;
    private Metrics metrics = new Metrics();
    private Timer t;

    /**
     * Creates a new search object for a given game.
     */
    public static <STATE, ACTION, PLAYER> AlphaBetaSearch<STATE, ACTION, PLAYER> createFor(Game<STATE, ACTION, PLAYER> game) {
        return new AlphaBetaSearch<STATE, ACTION, PLAYER>(game);
    }

    public MyAlphaBetaSearch(Game<S, A, P> game, int maxDepth, int time) {
        this.game = game;
        this.maxDepth = maxDepth;
        this.t= new Timer(time);
    }

    @Override
    public A makeDecision(S state) {
        metrics = new Metrics();
        A result = null;
        this.t.start();
        double resultValue = Double.NEGATIVE_INFINITY;
        P player = game.getPlayer(state);
        for (A action : game.getActions(state)) {
            double value = minValue(game.getResult(state, action), player,Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1);
            if (value > resultValue) {
                result = action;
                resultValue = value;
            }
        }
        return result;
    }

    public double maxValue(S state, P player, double alpha, double beta, int depth) {
        metrics.incrementInt(METRICS_NODES_EXPANDED);
      //  System.out.println("PROFONDITA: "+ depth);
        if (game.isTerminal(state) || depth > this.maxDepth || this.t.timeOutOccurred())
            return game.getUtility(state, player);
        double value = Double.NEGATIVE_INFINITY;
        int newDepth = depth + 1;
        for (A action : game.getActions(state)) {
            value = Math.max(value, minValue(game.getResult(state, action), player, alpha, beta, newDepth));
            if (value >= beta)
                return value;
            alpha = Math.max(alpha, value);
        }
        return value;
    }

    public double minValue(S state, P player, double alpha, double beta, int depth) {
        metrics.incrementInt(METRICS_NODES_EXPANDED);
        //System.out.println("PROFONDITA: "+ depth);
        if (game.isTerminal(state) || depth > this.maxDepth || this.t.timeOutOccurred())
            return game.getUtility(state, player);
        double value = Double.POSITIVE_INFINITY;
        int newDepth = depth + 1;
        for (A action : game.getActions(state)) {
            value = Math.min(value, maxValue(game.getResult(state, action), player, alpha, beta, newDepth));
            if (value <= alpha)
                return value;
            beta = Math.min(beta, value);
        }
        return value;
    }

    @Override
    public Metrics getMetrics() {
        return metrics;
    }
    
    
    private static class Timer {
        private long duration;
        private long startTime;

        Timer(int maxSeconds) {
            this.duration = 1000 * maxSeconds;
        }

        void start() {
            startTime = System.currentTimeMillis();
        }

        boolean timeOutOccurred() {
            return System.currentTimeMillis() > startTime + duration;
        }
    }
}