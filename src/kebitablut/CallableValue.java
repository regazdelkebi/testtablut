/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kebitablut;

import domain.Action;
import domain.StateTablut;
import java.util.List;
import java.util.concurrent.Callable;

/**
 *
 * @author matte
 */
public class CallableValue implements Callable<Double>{

    private MyGameTablut game;
    private StateTablut s;
    private Action a;
    public domain.State.Turn t;
    
    protected double utilMax;
    protected double utilMin;
    
    private int currDepthLimit;
    
    private MyIterativeDeepeningAlphaBetaSearch.Timer timer;
    
    public CallableValue(MyGameTablut g, StateTablut s, Action a, int depthLimit, domain.State.Turn t, MyIterativeDeepeningAlphaBetaSearch.Timer timer, double utilMin, double utilMax){
        this.game = g;
        this.s = s;
        this.a = a;
        this.t = t;
        
        this.timer = timer;
        
        this.utilMax = utilMax;
        this.utilMin = utilMin;
        
        this.currDepthLimit = depthLimit;
    }
    
    @Override
    public Double call() throws Exception {
        return minValue(game.getResult(s, a), t, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1);
    }
    
    // returns an utility value
    public double maxValue(StateTablut state, domain.State.Turn player, double alpha, double beta, int depth) {
        if (game.isTerminal(state) || depth >= currDepthLimit || timer.timeOutOccurred()) {
            return state.eval(player, depth);
        } else {
            double value = Double.NEGATIVE_INFINITY;
            for (Action action : game.getActions(state)) {
                value = Math.max(value, minValue(game.getResult(state, action), player, alpha, beta, depth + 1));
                if (value >= beta) {
                    return value;
                }
                alpha = Math.max(alpha, value);
            }
            return value;
        }
    }

    // returns an utility value
    public double minValue(StateTablut state, domain.State.Turn player, double alpha, double beta, int depth) {

        if (game.isTerminal(state) || depth >= currDepthLimit || timer.timeOutOccurred()) {
            return state.eval(player, depth);
        } else {
            double value = Double.POSITIVE_INFINITY;
            for (Action action : game.getActions(state)) {
                value = Math.min(value, maxValue(game.getResult(state, action), player, alpha, beta, depth + 1));
                if (value <= alpha) {
                    return value;
                }
                beta = Math.min(beta, value);
            }
            return value;
        }
    }
    
    /**
     * Primitive operation, which estimates the value for (not necessarily
     * terminal) states. This implementation returns the utility value for
     * terminal states and <code>(utilMin + utilMax) / 2</code> for non-terminal
     * states. When overriding, first call the super implementation!
     */
    protected double eval(StateTablut state, domain.State.Turn player, int depth) {
        if (game.isTerminal(state) || timer.timeOutOccurred() || depth >= currDepthLimit) {
            return game.getUtility(state, player);
        } else {
            return (utilMin + utilMax) / 2;
        }
    }

    /**
     * Primitive operation for action ordering. This implementation preserves
     * the original order (provided by the game).
     */
    public List<Action> orderActions(StateTablut state, List<Action> actions, domain.State.Turn player, int depth) {

        return actions;
    }
}
