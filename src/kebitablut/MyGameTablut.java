/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kebitablut;

import domain.Action;
import domain.GameTablut;
import domain.State;
import domain.StateTablut;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matte
 */
public class MyGameTablut extends GameTablut implements aima.core.search.adversarial.Game<StateTablut, Action, State.Turn> {

    public static void main(String[] args) {

        Action a = new Action(3, 3, 4, 6, State.Turn.WHITE);
//        System.out.println(a.toString() + "\n" + a.from + a.to);

        State s = new StateTablut();

        MyGameTablut g = new MyGameTablut();

        s.setTurn(State.Turn.WHITE);

//        System.out.println(s);        
//        List<Action> actions = g.getActions((StateTablut) s);
//
        System.out.println(s);
    }

    public StateTablut actualState;

    private double minValue;
    private double tempMinValue;

    public MyGameTablut() {
        super();
        this.actualState = new StateTablut();
    }

    @Override
    public StateTablut getInitialState() {
        return this.actualState;
    }

    @Override
    public State.Turn[] getPlayers() {
        State.Turn[] turns = new State.Turn[2];

        turns[0] = State.Turn.WHITE;
        turns[1] = State.Turn.BLACK;

        return turns;
    }

    @Override
    public State.Turn getPlayer(StateTablut state) {
        return state.getTurn();
    }

    @Override
    public List<Action> getActions(StateTablut state) {
        List<Action> myList = new ArrayList();

        int i, j;

        for (i = 0; i < 9; i++) {
            for (j = 0; j < 9; j++) {
                if (!state.getPawn(i, j).equalsPawn("E") && canMove(state.getPawn(i, j), state.getTurn())) {
                    myList.addAll(TryMoves(state, i, j));
                }
            }
        }
        
        return myList;
    }

    private boolean canMove(State.Pawn pawn, State.Turn turn) {
        return pawn.toString().equals(turn.toString()) || (pawn.equalsPawn("K") && turn.equalsTurn("W"));
    }

    @Override
    public StateTablut getResult(StateTablut state, Action action) {

        return (StateTablut) super.movePawn(state, action);
    }

    @Override
    public boolean isTerminal(StateTablut state) {
        switch (state.getTurn().toString()) {
            case ("W"):
                return false;
            case ("B"):
                return false;
            case ("D"):
                return false;
            case ("BW"):
                return true;
            case ("WW"):
                return true;
            default:
                return false;
        }
    }

    @Override
    public double getUtility(StateTablut state, State.Turn player) {
        //return state.eval(player);
        return 0.0;
    }

    private Collection<? extends Action> TryMoves(StateTablut state, int pawnX, int pawnY) {

        List<Action> myList = new ArrayList();

        this.minValue = 0.0;

        myList.addAll(TryHorizontalMoves(state, pawnX, pawnY, 1));
        myList.addAll(TryHorizontalMoves(state, pawnX, pawnY, -1));
        myList.addAll(TryVerticalMoves(state, pawnX, pawnY, 1));
        myList.addAll(TryVerticalMoves(state, pawnX, pawnY, -1));
        return myList;
    }

    private List<Action> TryHorizontalMoves(StateTablut state, int pawnX, int pawnY, int num) {

        List<Action> myList = new ArrayList();
        Action a = new Action(pawnX, pawnX, pawnY, pawnY + num, state.getTurn());

        int count = num;

        try {
            while (super.checkMove(state, a)) {
                myList.add(a);
                count = count + num;
                a = new Action(pawnX, pawnX, pawnY, pawnY + count, state.getTurn());
            }
        } catch (Exception ex) {
            Logger.getLogger(MyGameTablut.class.getName()).log(Level.SEVERE, null, ex);
        }

        return myList;
    }

    private List<Action> TryVerticalMoves(StateTablut state, int pawnX, int pawnY, int num) {
        List<Action> myList = new ArrayList();
        Action a = new Action(pawnX, pawnX + num, pawnY, pawnY, state.getTurn());
        int count = num;

        try {
            while (super.checkMove(state, a)) {
                myList.add(a);
                count = count + num;
                a = new Action(pawnX, pawnX + count, pawnY, pawnY, state.getTurn());
            }
        } catch (Exception ex) {
            Logger.getLogger(MyGameTablut.class.getName()).log(Level.SEVERE, null, ex);
        }

        return myList;
    }

}
