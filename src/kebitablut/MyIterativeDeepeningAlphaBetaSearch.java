package kebitablut;

import aima.core.search.adversarial.AdversarialSearch;
import aima.core.search.adversarial.Game;
import java.util.ArrayList;
import java.util.List;

import aima.core.search.framework.Metrics;
import domain.Action;
import domain.State;
import domain.StateTablut;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implements an iterative deepening Minimax search with alpha-beta pruning and
 * action ordering. Maximal computation time is specified in seconds. The
 * algorithm is implemented as template method and can be configured and tuned
 * by subclassing.
 *
 * @param <S> Type which is used for states in the game.
 * @param <A> Type which is used for actions in the game.
 * @param <P> Type which is used for players in the game.
 * @author Ruediger Lunde
 */
public class MyIterativeDeepeningAlphaBetaSearch<S extends StateTablut, A extends Action, P extends State.Turn> implements AdversarialSearch<S, A> {

    public final static String METRICS_NODES_EXPANDED = "nodesExpanded";
    public final static String METRICS_MAX_DEPTH = "maxDepth";

    protected Game<S, A, P> game;
    protected double utilMax;
    protected double utilMin;
    protected int currDepthLimit;
    private boolean heuristicEvaluationUsed; // indicates that non-terminal
    // nodes
    // have been evaluated.
    private Timer timer;
    private boolean logEnabled;

    private Metrics metrics = new Metrics();

    /**
     * Creates a new search object for a given game.
     *
     * @param game The game.
     * @param utilMin Utility value of worst state for this player. Supports
     * evaluation of non-terminal states and early termination in situations
     * with a safe winner.
     * @param utilMax Utility value of best state for this player. Supports
     * evaluation of non-terminal states and early termination in situations
     * with a safe winner.
     * @param time Maximal computation time in seconds.
     */
    public static <S, A, P> MyIterativeDeepeningAlphaBetaSearch<StateTablut, Action, State.Turn> createFor(Game<StateTablut, Action, State.Turn> game, double utilMin, double utilMax, int time) {
        return new MyIterativeDeepeningAlphaBetaSearch<>(game, utilMin, utilMax, time);
    }

    /**
     * Creates a new search object for a given game.
     *
     * @param game The game.
     * @param utilMin Utility value of worst state for this player. Supports
     * evaluation of non-terminal states and early termination in situations
     * with a safe winner.
     * @param utilMax Utility value of best state for this player. Supports
     * evaluation of non-terminal states and early termination in situations
     * with a safe winner.
     * @param time Maximal computation time in seconds.
     */
    public MyIterativeDeepeningAlphaBetaSearch(Game<S, A, P> game, double utilMin, double utilMax, int time) {
        this.game = game;
        this.utilMin = utilMin;
        this.utilMax = utilMax;
        this.timer = new Timer(time);

        this.logEnabled = false;

    }

    public void setLogEnabled(boolean b) {
        logEnabled = b;
    }

    /**
     * Template method controlling the search. It is based on iterative
     * deepening and tries to make to a good decision in limited time. Credit
     * goes to Behi Monsio who had the idea of ordering actions by utility in
     * subsequent depth-limited search runs.
     */
    @Override
    public A makeDecision(S state) {
        metrics = new Metrics();
        StringBuffer logText = null;
        P player = game.getPlayer(state);
        List<A> results = orderActions(state, game.getActions(state), player, 0);
        timer.start();
        currDepthLimit = 3;

        int i, index = 0;
        Future<Double>[] futureValues = new Future[results.size()];
        do {
            incrementDepthLimit();
            if (logEnabled) {
                logText = new StringBuffer("depth " + currDepthLimit + ": ");
            }
            ActionStore<A> newResults = new ActionStore<>();

            ExecutorService pool = Executors.newFixedThreadPool(results.size());
            i=0;
            for (A action : results) {

                futureValues[i] = pool.submit(new CallableValue((MyGameTablut) game, state, action, currDepthLimit, state.getTurn(), timer, utilMin, utilMax));
//                double value = minValue(game.getResult(state, action), player, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1);

//                newResults.add(action, value);
                if (logEnabled) {
//                    logText.append(action).append("->").append(value).append(" ");
                }

                if (timer.timeOutOccurred()) {
                    break; // exit from action loop
                }
                i++;
            }

            for(i=0;i<results.size();i++){
                Double value = 0.0;
                A action = results.get(i);
                try {
                    value = futureValues[i].get();
                } catch (Exception ex) {
                    Logger.getLogger(MyIterativeDeepeningAlphaBetaSearch.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                newResults.add(action, value);
                if (logEnabled) {
                    logText.append(action).append("->").append(value).append(" ");
                }
            }
            if (newResults.size() > 0 && !timer.timeOutOccurred()) {
//            System.out.println("depth " + currDepthLimit + " completed");
                results = newResults.actions;
                if (hasSafeWinner(newResults.utilValues.get(0))) {
                    break; // exit from iterative deepening loop
                } else if (newResults.size() > 1 && isSignificantlyBetter(newResults.utilValues.get(0), newResults.utilValues.get(1))) {
                    break; // exit from iterative deepening loop
                }

            }
        } while (!timer.timeOutOccurred());

//        System.out.println("Scelgo: " + results.get(0));
        return results.get(0);
    }

    // returns an utility value
    public double maxValue(S state, P player, double alpha, double beta, int depth) {
        updateMetrics(depth);

        if (game.isTerminal(state) || depth >= currDepthLimit || timer.timeOutOccurred()) {
            return state.eval(player, depth);
        } else {
            double value = Double.NEGATIVE_INFINITY;
            for (A action : game.getActions(state)) {
                value = Math.max(value, minValue(game.getResult(state, action), player, alpha, beta, depth + 1));
                if (value >= beta) {
                    return value;
                }
                alpha = Math.max(alpha, value);
            }
            return value;
        }
    }

    // returns an utility value
    public double minValue(S state, P player, double alpha, double beta, int depth) {
        updateMetrics(depth);

        if (game.isTerminal(state) || depth >= currDepthLimit || timer.timeOutOccurred()) {
            return state.eval(player, depth);
        } else {
            double value = Double.POSITIVE_INFINITY;
            for (A action : game.getActions(state)) {
                value = Math.min(value, maxValue(game.getResult(state, action), player, alpha, beta, depth + 1));
                if (value <= alpha) {
                    return value;
                }
                beta = Math.min(beta, value);
            }
            return value;
        }
    }

    private void updateMetrics(int depth) {
        metrics.incrementInt(METRICS_NODES_EXPANDED);
        metrics.set(METRICS_MAX_DEPTH, Math.max(metrics.getInt(METRICS_MAX_DEPTH), depth));
    }

    /**
     * Returns some statistic data from the last search.
     */
    @Override
    public Metrics getMetrics() {
        return metrics;
    }

    /**
     * Primitive operation which is called at the beginning of one depth limited
     * search step. This implementation increments the current depth limit by
     * one.
     */
    protected void incrementDepthLimit() {
        currDepthLimit++;
    }

    /**
     * Primitive operation which is used to stop iterative deepening search in
     * situations where a clear best action exists. This implementation returns
     * always false.
     */
    protected boolean isSignificantlyBetter(double newUtility, double utility) {
        return false;
    }

    /**
     * Primitive operation which is used to stop iterative deepening search in
     * situations where a safe winner has been identified. This implementation
     * returns true if the given value (for the currently preferred action
     * result) is the highest or lowest utility value possible.
     */
    protected boolean hasSafeWinner(double resultUtility) {
        return resultUtility <= utilMin || resultUtility >= utilMax;
    }

    /**
     * Primitive operation, which estimates the value for (not necessarily
     * terminal) states. This implementation returns the utility value for
     * terminal states and <code>(utilMin + utilMax) / 2</code> for non-terminal
     * states. When overriding, first call the super implementation!
     */
    protected double eval(S state, P player, int depth) {
        if (game.isTerminal(state) || timer.timeOutOccurred() || depth >= currDepthLimit) {
            return game.getUtility(state, player) - (depth-1);
        } else {
            heuristicEvaluationUsed = true;
            return (utilMin + utilMax) / 2;
        }
    }

    /**
     * Primitive operation for action ordering. This implementation preserves
     * the original order (provided by the game).
     */
    public List<A> orderActions(S state, List<A> actions, P player, int depth) {
        return actions;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // nested helper classes
    public static class Timer {

        private long duration;
        private long startTime;

        Timer(int maxSeconds) {
            this.duration = 1000 * maxSeconds;
        }

        void start() {
            startTime = System.currentTimeMillis();
        }

        boolean timeOutOccurred() {
            return System.currentTimeMillis() > startTime + duration;
        }
    }

    /**
     * Orders actions by utility.
     */
    private static class ActionStore<A> {

        private List<A> actions = new ArrayList<>();
        private List<Double> utilValues = new ArrayList<>();

        void add(A action, double utilValue) {
            int idx = 0;
            while (idx < actions.size() && utilValue <= utilValues.get(idx)) {
                idx++;
            }
            actions.add(idx, action);
            utilValues.add(idx, utilValue);
        }

        int size() {
            return actions.size();
        }

        public String toString() {
            int i;
            String s = new String();
            for (i = 0; i < actions.size(); i++) {
                s = s + "\n" + actions.get(i) + " -> " + utilValues.get(i);
            }

            return s;
        }
    }

    private class ActionComparator implements Comparator<A> {

        private ActionStore<A> newResults;

        public ActionComparator(ActionStore<A> newResults) {
            this.newResults = newResults;
        }

        @Override
        public int compare(A o1, A o2) {
            return (int) (newResults.utilValues.get(newResults.actions.indexOf(o2)) - newResults.utilValues.get(newResults.actions.indexOf(o1)));
        }
    }
}
