package domain;

import exceptions.ActionException;
import exceptions.BoardException;
import exceptions.ClimbingException;
import exceptions.DiagonalException;
import exceptions.OccupitedException;
import exceptions.PawnException;
import exceptions.StopException;
import exceptions.ThroneException;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import kebitablut.MyGameTablut;

/**
 * This class represents a state of a match of Tablut (classical or second
 * version)
 *
 * @author A.Piretti
 *
 */
public class StateTablut extends State implements Serializable {

    private static final long serialVersionUID = 1L;

    public static void main(String[] args) {
        StateTablut s = new StateTablut();

        MyGameTablut g = new MyGameTablut();
        
        s.removePawn(4, 4);
        s.removePawn(2, 4);
        s.removePawn(4, 2);
        s.removePawn(4, 6);
        s.removePawn(4, 7);

        s.getBoard()[5][6] = State.Pawn.KING;
        s.getBoard()[6][6] = State.Pawn.BLACK;
        s.getBoard()[4][4] = State.Pawn.THRONE;
        Action a = null;
        try {
            a = new Action("B5", "C5", State.Turn.BLACK);
        } catch (IOException ex) {
            Logger.getLogger(StateTablut.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println(s.eval(Turn.WHITE, 0));
        try {
            System.out.println(g.checkMove(s, a));
        } catch (Exception ex) {
            Logger.getLogger(StateTablut.class.getName()).log(Level.SEVERE, null, ex);
        }
        s = (StateTablut) g.movePawn(s, a);
        System.out.println(s);
    }

    public StateTablut() {
        super();
        this.board = new Pawn[9][9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                this.board[i][j] = Pawn.EMPTY;
            }
        }

        this.board[4][4] = Pawn.THRONE;

        this.turn = Turn.BLACK;

        this.board[4][4] = Pawn.KING;

        this.board[2][4] = Pawn.WHITE;
        this.board[3][4] = Pawn.WHITE;
        this.board[5][4] = Pawn.WHITE;
        this.board[6][4] = Pawn.WHITE;
        this.board[4][2] = Pawn.WHITE;
        this.board[4][3] = Pawn.WHITE;
        this.board[4][5] = Pawn.WHITE;
        this.board[4][6] = Pawn.WHITE;

        this.board[0][3] = Pawn.BLACK;
        this.board[0][4] = Pawn.BLACK;
        this.board[0][5] = Pawn.BLACK;
        this.board[1][4] = Pawn.BLACK;
        this.board[8][3] = Pawn.BLACK;
        this.board[8][4] = Pawn.BLACK;
        this.board[8][5] = Pawn.BLACK;
        this.board[7][4] = Pawn.BLACK;
        this.board[3][0] = Pawn.BLACK;
        this.board[4][0] = Pawn.BLACK;
        this.board[5][0] = Pawn.BLACK;
        this.board[4][1] = Pawn.BLACK;
        this.board[3][8] = Pawn.BLACK;
        this.board[4][8] = Pawn.BLACK;
        this.board[5][8] = Pawn.BLACK;
        this.board[4][7] = Pawn.BLACK;

    }

    public StateTablut clone() {
        StateTablut result = new StateTablut();

        Pawn oldboard[][] = this.getBoard();
        Pawn newboard[][] = result.getBoard();

        for (int i = 0; i < this.board.length; i++) {
            System.arraycopy(oldboard[i], 0, newboard[i], 0, this.board[i].length);
        }

        result.setBoard(newboard);
        result.setTurn(this.turn);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        StateTablut other = (StateTablut) obj;
        if (this.board == null) {
            if (other.board != null) {
                return false;
            }
        } else {
            if (other.board == null) {
                return false;
            }
            if (this.board.length != other.board.length) {
                return false;
            }
            if (this.board[0].length != other.board[0].length) {
                return false;
            }
            for (int i = 0; i < other.board.length; i++) {
                for (int j = 0; j < other.board[i].length; j++) {
                    if (!this.board[i][j].equals(other.board[i][j])) {
                        return false;
                    }
                }
            }
        }
        return this.turn == other.turn;
    }

    public double eval(State.Turn player, int depth) {

        int i, j;
        int l = this.getBoard().length;
        int kingX = -1, kingY = -1;
        int countBlack = 0, countWhite = 0, aroundCount = 0;

        for (i = 0; i < l; i++) {
            for (j = 0; j < l; j++) {
                switch (this.getBoard()[i][j].toString()) {
                    case ("W"):
                        countWhite++;
                        break;
                    case ("B"):
                        countBlack++;
                        break;
                    case ("K"):
                        countWhite++;
                        kingX = i;
                        kingY = j;
                        break;
                    default:
                        break;
                }
            }
        }

        if (kingY != -1 && kingY != -1) {
            if (kingY != 8 && this.getPawn(kingX, kingY + 1).equals(State.Pawn.BLACK)) {
                aroundCount++;
            }

            if (kingY != 0 && this.getPawn(kingX, kingY - 1).equals(State.Pawn.BLACK)) {
                aroundCount++;
            }

            if (kingX != 8 && this.getPawn(kingX + 1, kingY).equals(State.Pawn.BLACK)) {
                aroundCount++;
            }

            if (kingX != 0 && this.getPawn(kingX - 1, kingY).equals(State.Pawn.BLACK)) {
                aroundCount++;
            }
        } else {
            aroundCount = 0;
        }

        int countVal = 2 * (2*(countWhite) + (16 - countBlack));

        double distanceVal = 15 * waysOut(kingX, kingY);

        double val = countVal + distanceVal + 2 * (4 - aroundCount);

        if (player.equalsTurn("W")) {
            if (this.turn.equalsTurn("WW")) {
                return 180.0 - depth;
            }

            if (this.turn.equalsTurn("BW")) {
                return 10.0;
            }

            return val;
        } else {
            if (this.turn.equalsTurn("BW")) {
                return 180.0 - depth;
            }

            if (this.turn.equalsTurn("WW")) {
                return 10.0;
            }

            return 170 - val;
        }

    }
    
    private int waysOut(int x, int y){
        int count = 0;

        count+=TryHorizontalMoves(x, y, 1);
        count+=TryHorizontalMoves(x, y, -1);
        count+=TryVerticalMoves(x, y, 1);
        count+=TryVerticalMoves(x, y, -1);
        
       
        return count;
    }

    private int TryHorizontalMoves(int pawnX, int pawnY, int num) {
        Action a = new Action(pawnX, pawnX, pawnY, pawnY + num, this.getTurn());
        int count = num;
        int winCount=0;
        try {
            while (this.checkMove(this, a)) {
                if(this.movePawn(this, a).turn.equalsTurn("WW")){
                    winCount++;
                }
                
                count = count + num;
                a = new Action(pawnX, pawnX, pawnY, pawnY + count, this.getTurn());
            }
        } catch (Exception ex) {
            Logger.getLogger(MyGameTablut.class.getName()).log(Level.SEVERE, null, ex);
        }
        return winCount;
    }

    private int TryVerticalMoves(int pawnX, int pawnY, int num) {
        Action a = new Action(pawnX, pawnX + num, pawnY, pawnY, this.getTurn());
        int count = num;
        int winCount=0;
        try {
            while (this.checkMove(this, a)) {
                
                if(this.movePawn(this, a).turn.equalsTurn("WW")){
                    winCount++;
                }
                count = count + num;
                a = new Action(pawnX, pawnX + count, pawnY, pawnY, this.getTurn());
            }
        } catch (Exception ex) {
            Logger.getLogger(MyGameTablut.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return winCount;
    }
    
    public boolean checkMove(State state, Action a) throws BoardException, ActionException, StopException, PawnException, DiagonalException, ClimbingException, ThroneException, OccupitedException {
        //this.loggGame.fine(a.toString());
        //controllo la mossa
        /*if (a.getTo().length() != 2 || a.getFrom().length() != 2) {
            this.loggGame.warning("Formato mossa errato");
            throw new ActionException(a);
        }*/
        int columnFrom = a.getColumnFrom();
        int columnTo = a.getColumnTo();
        int rowFrom = a.getRowFrom();
        int rowTo = a.getRowTo();

        //controllo se sono fuori dal tabellone
        if (columnFrom > state.getBoard().length - 1 || rowFrom > state.getBoard().length - 1 || rowTo > state.getBoard().length - 1 || columnTo > state.getBoard().length - 1 || columnFrom < 0 || rowFrom < 0 || rowTo < 0 || columnTo < 0) {
//            System.out.println("Mossa fuori tabellone");
            return false;
        }

        //controllo che non vada sul trono
        if (state.getPawn(rowTo, columnTo).equalsPawn(State.Pawn.THRONE.toString())) {
//            this.loggGame.warning("Mossa sul trono");
            return false;
        }

        //controllo la casella di arrivo
        if (!state.getPawn(rowTo, columnTo).equalsPawn(State.Pawn.EMPTY.toString())) {
//            System.out.println("Mossa sopra una casella occupata");
            return false;
        }

        //controllo se cerco di stare fermo
        if (rowFrom == rowTo && columnFrom == columnTo) {
//            System.out.println("Nessuna mossa");
            return false;
        }

        //controllo se sto muovendo una pedina giusta
        if (state.getTurn().equalsTurn(State.Turn.WHITE.toString())) {
            if (!state.getPawn(rowFrom, columnFrom).equalsPawn("W") && !state.getPawn(rowFrom, columnFrom).equalsPawn("K")) {
//                System.out.println("Giocatore " + a.getTurn() + " cerca di muovere una pedina avversaria");
                return false;
            }
        }
        if (state.getTurn().equalsTurn(State.Turn.BLACK.toString())) {
            if (!state.getPawn(rowFrom, columnFrom).equalsPawn("B")) {
//                System.out.println("Giocatore " + a.getTurn() + " cerca di muovere una pedina avversaria");
                return false;
            }
        }

        //controllo di non muovere in diagonale
        if (rowFrom != rowTo && columnFrom != columnTo) {
//            System.out.println("Mossa in diagonale");
            return false;
        }

        //controllo di non scavalcare pedine
        if (rowFrom == rowTo) {
            if (columnFrom > columnTo) {
                for (int i = columnTo; i < columnFrom; i++) {
                    if (!state.getPawn(rowFrom, i).equalsPawn(State.Pawn.EMPTY.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            } else {
                for (int i = columnFrom + 1; i <= columnTo; i++) {
                    if (!state.getPawn(rowFrom, i).equalsPawn(State.Pawn.EMPTY.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            }
        } else {
            if (rowFrom > rowTo) {
                for (int i = rowTo; i < rowFrom; i++) {
                    if (!state.getPawn(i, columnFrom).equalsPawn(State.Pawn.EMPTY.toString()) && !state.getPawn(i, columnFrom).equalsPawn(State.Pawn.THRONE.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            } else {
                for (int i = rowFrom + 1; i <= rowTo; i++) {
                    if (!state.getPawn(i, columnFrom).equalsPawn(State.Pawn.EMPTY.toString()) && !state.getPawn(i, columnFrom).equalsPawn(State.Pawn.THRONE.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            }
        }
        //se sono arrivato qui, muovo la pedina
        
        if(!isCamp(rowFrom,columnFrom) && isCamp(rowTo, columnTo)){
            return false;
        }
        
        return true;
    }

    private boolean isCamp(int x, int y) {

        if (x == 0 && (y < 6 && y > 2)) {
            return true;
        }

        if (x == 8 && (y < 6 && y > 2)) {
            return true;
        }

        if (y == 0 && (x < 6 && x > 2)) {
            return true;
        }

        if (y == 8 && (x < 6 && x > 2)) {
            return true;
        }

        if ((y == 1 || y == 7) && x == 4) {
            return true;
        }

        if ((x == 1 || x == 7) && y == 4) {
            return true;
        }

        return false;
    }
    
    public State movePawn(State state, Action a) {

        State s = state.clone();

        State.Pawn pawn = state.getPawn(a.getRowFrom(), a.getColumnFrom());
        State.Pawn[][] newBoard = s.getBoard();
        /*this.loggGame.fine("Movimento pedina");*/
        if (newBoard.length == 9) {
            if (a.getColumnFrom() == 4 && a.getRowFrom() == 4) {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.THRONE;
            } else {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.EMPTY;
            }
        }
        /*if (newBoard.length == 7) {
            if (a.getColumnFrom() == 3 && a.getRowFrom() == 3) {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.THRONE;
            } else {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.EMPTY;
            }
        }*/

        //metto nel nuovo tabellone la pedina mossa
        newBoard[a.getRowTo()][a.getColumnTo()] = pawn;

        //cambio il turno
        if (state.getTurn().equalsTurn(State.Turn.WHITE.toString())) {
            s.setTurn(State.Turn.BLACK);
            s = checkCaptureWhite(s, a);
        } else {
            s.setTurn(State.Turn.WHITE);
            s = checkCaptureBlack(s, a);
        }
        return s;
    }

    /**
     * This method check if a pawn is captured and if the game ends
     *
     * @param state the state of the game
     * @param a the action of the previous moved pawn
     * @return the new state of the game
     */
    private State checkCaptureWhite(State state, Action a) {
        //controllo se mangio a destra
        if (a.getColumnTo() < state.getBoard().length - 2 && state.getPawn(a.getRowTo(), a.getColumnTo() + 1).equalsPawn("B") && (state.getPawn(a.getRowTo(), a.getColumnTo() + 2).equalsPawn("W") || state.getPawn(a.getRowTo(), a.getColumnTo() + 2).equalsPawn("T") || state.getPawn(a.getRowTo(), a.getColumnTo() + 2).equalsPawn("K") || isCamp(a.rowTo, a.columnTo + 2))) {
            state.removePawn(a.getRowTo(), a.getColumnTo() + 1);
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo(), a.getColumnTo() + 1));
        }
        //controllo se mangio a sinistra
        if (a.getColumnTo() > 1 && state.getPawn(a.getRowTo(), a.getColumnTo() - 1).equalsPawn("B") && (state.getPawn(a.getRowTo(), a.getColumnTo() - 2).equalsPawn("W") || state.getPawn(a.getRowTo(), a.getColumnTo() - 2).equalsPawn("T") || state.getPawn(a.getRowTo(), a.getColumnTo() - 2).equalsPawn("K") || isCamp(a.rowTo, a.columnTo - 2))) {
            state.removePawn(a.getRowTo(), a.getColumnTo() - 1);
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo(), a.getColumnTo() - 1));
        }
        //controllo se mangio sopra
        if (a.getRowTo() > 1 && state.getPawn(a.getRowTo() - 1, a.getColumnTo()).equalsPawn("B") && (state.getPawn(a.getRowTo() - 2, a.getColumnTo()).equalsPawn("W") || state.getPawn(a.getRowTo() - 2, a.getColumnTo()).equalsPawn("T") || state.getPawn(a.getRowTo() - 2, a.getColumnTo()).equalsPawn("K") || isCamp(a.rowTo - 2, a.columnTo))) {
            state.removePawn(a.getRowTo() - 1, a.getColumnTo());
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo() - 1, a.getColumnTo()));
        }
        //controllo se mangio sotto
        if (a.getRowTo() < state.getBoard().length - 2 && state.getPawn(a.getRowTo() + 1, a.getColumnTo()).equalsPawn("B") && (state.getPawn(a.getRowTo() + 2, a.getColumnTo()).equalsPawn("W") || state.getPawn(a.getRowTo() + 2, a.getColumnTo()).equalsPawn("T") || state.getPawn(a.getRowTo() + 2, a.getColumnTo()).equalsPawn("K") || isCamp(a.rowTo + 2, a.columnTo))) {
            state.removePawn(a.getRowTo() + 1, a.getColumnTo());
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo() + 1, a.getColumnTo()));
        }
        //controllo se ho vinto
        if (a.getRowTo() == 0 || a.getRowTo() == state.getBoard().length - 1 || a.getColumnTo() == 0 || a.getColumnTo() == state.getBoard().length - 1) {
            if (state.getPawn(a.getRowTo(), a.getColumnTo()).equalsPawn("K")) {
                state.setTurn(State.Turn.WHITEWIN);
//                this.loggGame.fine("Bianco vince con re in " + a.getTo());
            }
        }

        //controllo il pareggio
//        if (this.movesWithutCapturing >= this.movesDraw && (state.getTurn().equalsTurn("B") || state.getTurn().equalsTurn("W"))) {
//            state.setTurn(State.Turn.DRAW);
//            this.loggGame.fine("Stabilito un pareggio per troppe mosse senza mangiare");
//        }
//        this.movesWithutCapturing++;
        return state;
    }

    /**
     * This method check if a pawn is captured and if the game ends
     *
     * @param state the state of the game
     * @param a the action of the previous moved pawn
     * @return the new state of the game
     */
    private State checkCaptureBlack(State state, Action a) {
        
        int rowTo = a.getRowTo();
        int columnTo = a.getColumnTo();
        
        //Mangio pedina bianca sopra
        if( (rowTo > 1) &&
            state.getPawn(rowTo - 1, columnTo).equalsPawn("W") &&
            (
                state.getPawn(rowTo - 2, columnTo).equalsPawn("B") ||
                isCamp(rowTo - 2, columnTo) ||
                state.getPawn(rowTo - 2, columnTo).equalsPawn("T")
            )){
            state.removePawn(rowTo - 1, columnTo);
        }
        //Mangio pedina bianca sotto
        if( (rowTo < 7) &&
            state.getPawn(rowTo + 1, columnTo).equalsPawn("W") &&
            (
                state.getPawn(rowTo + 2, columnTo).equalsPawn("B") ||
                isCamp(rowTo + 2, columnTo) ||
                state.getPawn(rowTo + 2, columnTo).equalsPawn("T")
            )){
            state.removePawn(rowTo + 1, columnTo);
        }
        //Mangio pedina bianca a destra
        if( (columnTo < 7) &&
            state.getPawn(rowTo, columnTo + 1).equalsPawn("W") &&
            (
                state.getPawn(rowTo, columnTo + 2).equalsPawn("B") ||
                isCamp(rowTo, columnTo + 2) ||
                state.getPawn(rowTo, columnTo + 2).equalsPawn("T")
            )){
            state.removePawn(rowTo, columnTo + 1);
        }
        //Mangio pedina bianca a sinistra
        if( (columnTo > 1) &&
            state.getPawn(rowTo, columnTo - 1).equalsPawn("W") &&
            (
                state.getPawn(rowTo, columnTo - 2).equalsPawn("B") ||
                isCamp(rowTo, columnTo - 2) ||
                state.getPawn(rowTo, columnTo - 2).equalsPawn("T")
            )){
            state.removePawn(rowTo, columnTo - 1);
        }
        
        //Trono - Special case
        //Sopra
        if((rowTo > 1) && state.getPawn(rowTo - 1, columnTo).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo - 1, columnTo)){
                    if(state.aroundKingPieces(rowTo - 1, columnTo) == 3){
                        state.removePawn(rowTo - 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo - 2, columnTo).equalsPawn("B") || isCamp(rowTo - 2, columnTo)){
                        state.removePawn(rowTo - 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //Sotto
        if((rowTo < 7) && state.getPawn(rowTo + 1, columnTo).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo + 1, columnTo)){
                    if(state.aroundKingPieces(rowTo + 1, columnTo) == 3){
                        state.removePawn(rowTo + 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo + 2, columnTo).equalsPawn("B") || isCamp(rowTo + 2, columnTo)){
                        state.removePawn(rowTo + 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //Destra
        if((columnTo < 7) && state.getPawn(rowTo, columnTo + 1).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo, columnTo + 1)){
                    if(state.aroundKingPieces(rowTo, columnTo + 1) == 3){
                        state.removePawn(rowTo, columnTo + 1);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo, columnTo + 2).equalsPawn("B") || isCamp(rowTo, columnTo + 2)){
                        state.removePawn(rowTo, columnTo + 1);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //Sinistra
        if((columnTo > 1) && state.getPawn(rowTo, columnTo - 1).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo, columnTo - 1)){
                    if(state.aroundKingPieces(rowTo, columnTo - 1) == 3){
                        state.removePawn(rowTo, columnTo - 1);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo, columnTo - 2).equalsPawn("B") || isCamp(rowTo, columnTo - 2)){
                        state.removePawn(rowTo, columnTo - 1);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //controllo il re completamente circondato
        if (state.getPawn(4, 4).equalsPawn(State.Pawn.KING.toString()) && state.getBoard().length == 9) {
            if (state.getPawn(3, 4).equalsPawn("B") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B")) {
                state.setTurn(State.Turn.BLACKWIN);
            }
        }
        
        //controllo regola 11
        if (state.getBoard().length == 9) {
            if (a.getColumnTo() == 4 && a.getRowTo() == 2) {
                if (state.getPawn(3, 4).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B")) {
                    state.removePawn(3, 4);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(3, 4));
                }
            }
            if (a.getColumnTo() == 4 && a.getRowTo() == 6) {
                if (state.getPawn(5, 4).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B") && state.getPawn(3, 4).equalsPawn("B")) {
                    state.removePawn(5, 4);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(5, 4));
                }
            }
            if (a.getColumnTo() == 2 && a.getRowTo() == 4) {
                if (state.getPawn(4, 3).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(3, 4).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B")) {
                    state.removePawn(4, 3);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(4, 3));
                }
            }
            if (a.getColumnTo() == 6 && a.getRowTo() == 4) {
                if (state.getPawn(4, 5).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B") && state.getPawn(3, 4).equalsPawn("B")) {
                    state.removePawn(4, 5);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(4, 5));
                }
            }
        }
        return state;
    }
    
    private boolean isNearThrone(State state, int x,int y){
        if(state.getPawn(x + 1, y).equalsPawn("T") || state.getPawn(x - 1, y).equalsPawn("T") || state.getPawn(x, y + 1).equalsPawn("T") || state.getPawn(x, y - 1).equalsPawn("T")){
            return true;
        }
        return false;
    }

    static double DistanceFromWin(int x, int y) {
        int cornerX;
        int cornerY;

        if (x > 4) {
            cornerX = 8;
        } else {
            cornerX = 0;
        }

        if (y >= 4) {
            cornerY = 8;
        } else {
            cornerY = 0;
        }

        return Math.sqrt(Math.pow((cornerX - x), 2) + Math.pow((cornerY - y), 2));
    }
}
