package domain;

import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.Comparator;

/**
 * this class represents an action of a player
 *
 * @author A.Piretti
 *
 */
public class Action implements Serializable, aima.core.agent.Action{

    private static final long serialVersionUID = 1L;

    public String from;
    public String to;

    public int rowFrom;
    public int rowTo;
    public int columnFrom;
    public int columnTo;

    private State.Turn turn;

    public Action(String from, String to, StateTablut.Turn t) throws IOException {
        if (from.length() != 2 || to.length() != 2) {
            throw new InvalidParameterException("the FROM and the TO string must have length=2");
        } else {
            this.from = from;
            this.to = to;
            this.turn = t;

            this.rowFrom = Integer.parseInt(this.from.charAt(1) + "") - 1;
            this.rowTo = Integer.parseInt(this.to.charAt(1) + "") - 1;
            this.columnFrom = Character.toLowerCase(this.from.charAt(0)) - 97;
            this.columnTo = Character.toLowerCase(this.to.charAt(0)) - 97;
        }
    }

    public Action(int rowFrom, int rowTo, int columnFrom, int columnTo, StateTablut.Turn t) {
        this.rowFrom = rowFrom;
        this.rowTo = rowTo;
        this.columnFrom = columnFrom;
        this.columnTo = columnTo;
        this.turn = t;
        
        char[] charFrom = new char[2]; 
        charFrom[0] = (char) ((char)columnFrom + 65);
        charFrom[1] = (char) ((char)rowFrom + 49);
        
        char[] charTo = new char[2]; 
        charTo[0] = (char) ((char)columnTo + 65);
        charTo[1] = (char) ((char)rowTo + 49);
        
        this.from = new String(charFrom);
        this.to = new String(charTo);
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public StateTablut.Turn getTurn() {
        return turn;
    }

    public void setTurn(StateTablut.Turn turn) {
        this.turn = turn;
    }

    public String toString() {
        return "Turn: " + this.turn + " " + "Pawn from " + from + " to " + to;
    }

    /**
     * @return means the index of the column where the pawn is moved from
     */
    public int getColumnFrom() {
        //return Character.toLowerCase(this.from.charAt(0)) - 97;
        return this.columnFrom;
    }

    /**
     * @return means the index of the column where the pawn is moved to
     */
    public int getColumnTo() {
        return this.columnTo; //return Character.toLowerCase(this.to.charAt(0)) - 97;
    }

    /**
     * @return means the index of the row where the pawn is moved from
     */
    public int getRowFrom() {
        return this.rowFrom; //return Integer.parseInt(this.from.charAt(1) + "") - 1;
    }

    /**
     * @return means the index of the row where the pawn is moved to
     */
    public int getRowTo() {
        return this.rowTo; //return Integer.parseInt(this.to.charAt(1) + "") - 1;
    }

    @Override
    public boolean isNoOp() {
        return false;
    }

}
