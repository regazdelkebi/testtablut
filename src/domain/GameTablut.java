package domain;

import java.io.File;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import exceptions.*;

/**
 *
 * This class represents the pure game, with all the rules; it check the move,
 * the state of the match and is a pawn is eliminated or not
 *
 * @author A.Piretti
 *
 */
public class GameTablut implements Game {

//    private int movesDraw;
    private int movesWithutCapturing;

    public GameTablut() {
        this(0);
    }

    public GameTablut(int moves) {
        super();
//        this.movesDraw = moves;
        this.movesWithutCapturing = 0;
//        this.gameLogName = (new Date().getTime()) + "_gameLog.txt";
//        this.setGameLog(new File(this.gameLogName));
//        fh = null;
//        try {
//            fh = new FileHandler(gameLogName, true);
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.exit(1);
//        }
//        this.loggGame = Logger.getLogger("GameLog");
//        loggGame.addHandler(this.fh);
//        this.fh.setFormatter(new SimpleFormatter());
//        loggGame.setLevel(Level.FINE);
//        loggGame.fine("Inizio partita");
    }

    /**
     * This method checks an action in a state: if it is correct the state is
     * going to be changed, if it is wrong it throws a specific exception
     *
     * @param state the state of the game
     * @param a the action to be analyzed
     * @return the new state of the game
     * @throws BoardException try to move a pawn out of the board
     * @throws ActionException the format of the action is wrong
     * @throws StopException try to not move any pawn
     * @throws PawnException try to move an enemy pawn
     * @throws DiagonalException try to move a pawn diagonally
     * @throws ClimbingException try to climb over another pawn
     * @throws ThroneException try to move a pawn into the throne box
     * @throws OccupitedException try to move a pawn into an ccupited box
     */
    @Override
    public boolean checkMove(State state, Action a) throws BoardException, ActionException, StopException, PawnException, DiagonalException, ClimbingException, ThroneException, OccupitedException {
        //this.loggGame.fine(a.toString());
        //controllo la mossa
        /*if (a.getTo().length() != 2 || a.getFrom().length() != 2) {
            this.loggGame.warning("Formato mossa errato");
            throw new ActionException(a);
        }*/
        int columnFrom = a.getColumnFrom();
        int columnTo = a.getColumnTo();
        int rowFrom = a.getRowFrom();
        int rowTo = a.getRowTo();

        //controllo se sono fuori dal tabellone
        if (columnFrom > state.getBoard().length - 1 || rowFrom > state.getBoard().length - 1 || rowTo > state.getBoard().length - 1 || columnTo > state.getBoard().length - 1 || columnFrom < 0 || rowFrom < 0 || rowTo < 0 || columnTo < 0) {
//            System.out.println("Mossa fuori tabellone");
            return false;
        }

        //controllo che non vada sul trono
        if (state.getPawn(rowTo, columnTo).equalsPawn(State.Pawn.THRONE.toString())) {
//            System.out.println("Mossa sul trono");
            return false;
        }

        //controllo la casella di arrivo
        if (!state.getPawn(rowTo, columnTo).equalsPawn(State.Pawn.EMPTY.toString())) {
//            System.out.println("Mossa sopra una casella occupata");
            return false;
        }

        //controllo se cerco di stare fermo
        if (rowFrom == rowTo && columnFrom == columnTo) {
//            System.out.println("Nessuna mossa");
            return false;
        }

        //controllo se sto muovendo una pedina giusta
        if (state.getTurn().equalsTurn(State.Turn.WHITE.toString())) {
            if (!state.getPawn(rowFrom, columnFrom).equalsPawn("W") && !state.getPawn(rowFrom, columnFrom).equalsPawn("K")) {
//                System.out.println("Giocatore " + a.getTurn() + " cerca di muovere una pedina avversaria");
                return false;
            }
        }
        if (state.getTurn().equalsTurn(State.Turn.BLACK.toString())) {
            if (!state.getPawn(rowFrom, columnFrom).equalsPawn("B")) {
//                System.out.println("Giocatore " + a.getTurn() + " cerca di muovere una pedina avversaria");
                return false;
            }
        }

        //controllo di non muovere in diagonale
        if (rowFrom != rowTo && columnFrom != columnTo) {
//            System.out.println("Mossa in diagonale");
            return false;
        }

        //controllo di non scavalcare pedine
        if (rowFrom == rowTo) {
            if (columnFrom > columnTo) {
                for (int i = columnTo; i < columnFrom; i++) {
                    if (!state.getPawn(rowFrom, i).equalsPawn(State.Pawn.EMPTY.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            } else {
                for (int i = columnFrom + 1; i <= columnTo; i++) {
                    if (!state.getPawn(rowFrom, i).equalsPawn(State.Pawn.EMPTY.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            }
        } else {
            if (rowFrom > rowTo) {
                for (int i = rowTo; i < rowFrom; i++) {
                    if (!state.getPawn(i, columnFrom).equalsPawn(State.Pawn.EMPTY.toString()) && !state.getPawn(i, columnFrom).equalsPawn(State.Pawn.THRONE.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            } else {
                for (int i = rowFrom + 1; i <= rowTo; i++) {
                    if (!state.getPawn(i, columnFrom).equalsPawn(State.Pawn.EMPTY.toString()) && !state.getPawn(i, columnFrom).equalsPawn(State.Pawn.THRONE.toString())) {
//                        System.out.println("Mossa che scavalca una pedina");
                        return false;
                    }
                }
            }
        }
        //se sono arrivato qui, muovo la pedina
        
        if(!isCamp(rowFrom,columnFrom) && isCamp(rowTo, columnTo)){
            return false;
        }
        
        return true;
    }

    private boolean isCamp(int x, int y) {

        if (x == 0 && (y < 6 && y > 2)) {
            return true;
        }

        if (x == 8 && (y < 6 && y > 2)) {
            return true;
        }

        if (y == 0 && (x < 6 && x > 2)) {
            return true;
        }

        if (y == 8 && (x < 6 && x > 2)) {
            return true;
        }

        if ((y == 1 || y == 7) && x == 4) {
            return true;
        }

        if ((x == 1 || x == 7) && y == 4) {
            return true;
        }

        return false;
    }

    /**
     * This method move the pawn in the board
     *
     * @param state is the initial state
     * @param a is the action of a pawn
     * @return is the new state of the game with the moved pawn
     */
    public State movePawn(State state, Action a) {

        State s = state.clone();

        State.Pawn pawn = state.getPawn(a.getRowFrom(), a.getColumnFrom());
        State.Pawn[][] newBoard = s.getBoard();
        /*this.loggGame.fine("Movimento pedina");*/
        if (newBoard.length == 9) {
            if (a.getColumnFrom() == 4 && a.getRowFrom() == 4) {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.THRONE;
            } else {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.EMPTY;
            }
        }
        /*if (newBoard.length == 7) {
            if (a.getColumnFrom() == 3 && a.getRowFrom() == 3) {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.THRONE;
            } else {
                newBoard[a.getRowFrom()][a.getColumnFrom()] = State.Pawn.EMPTY;
            }
        }*/

        //metto nel nuovo tabellone la pedina mossa
        newBoard[a.getRowTo()][a.getColumnTo()] = pawn;

        //cambio il turno
        if (state.getTurn().equalsTurn(State.Turn.WHITE.toString())) {
            s.setTurn(State.Turn.BLACK);
            s = checkCaptureWhite(s, a);
        } else {
            s.setTurn(State.Turn.WHITE);
            s = checkCaptureBlack(s, a);
        }
        return s;
    }

    /**
     * This method check if a pawn is captured and if the game ends
     *
     * @param state the state of the game
     * @param a the action of the previous moved pawn
     * @return the new state of the game
     */
    private State checkCaptureWhite(State state, Action a) {
        //controllo se mangio a destra
        if (a.getColumnTo() < state.getBoard().length - 2 && state.getPawn(a.getRowTo(), a.getColumnTo() + 1).equalsPawn("B") && (state.getPawn(a.getRowTo(), a.getColumnTo() + 2).equalsPawn("W") || state.getPawn(a.getRowTo(), a.getColumnTo() + 2).equalsPawn("T") || state.getPawn(a.getRowTo(), a.getColumnTo() + 2).equalsPawn("K") || isCamp(a.rowTo, a.columnTo + 2))) {
            state.removePawn(a.getRowTo(), a.getColumnTo() + 1);
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo(), a.getColumnTo() + 1));
        }
        //controllo se mangio a sinistra
        if (a.getColumnTo() > 1 && state.getPawn(a.getRowTo(), a.getColumnTo() - 1).equalsPawn("B") && (state.getPawn(a.getRowTo(), a.getColumnTo() - 2).equalsPawn("W") || state.getPawn(a.getRowTo(), a.getColumnTo() - 2).equalsPawn("T") || state.getPawn(a.getRowTo(), a.getColumnTo() - 2).equalsPawn("K") || isCamp(a.rowTo, a.columnTo - 2))) {
            state.removePawn(a.getRowTo(), a.getColumnTo() - 1);
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo(), a.getColumnTo() - 1));
        }
        //controllo se mangio sopra
        if (a.getRowTo() > 1 && state.getPawn(a.getRowTo() - 1, a.getColumnTo()).equalsPawn("B") && (state.getPawn(a.getRowTo() - 2, a.getColumnTo()).equalsPawn("W") || state.getPawn(a.getRowTo() - 2, a.getColumnTo()).equalsPawn("T") || state.getPawn(a.getRowTo() - 2, a.getColumnTo()).equalsPawn("K") || isCamp(a.rowTo - 2, a.columnTo))) {
            state.removePawn(a.getRowTo() - 1, a.getColumnTo());
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo() - 1, a.getColumnTo()));
        }
        //controllo se mangio sotto
        if (a.getRowTo() < state.getBoard().length - 2 && state.getPawn(a.getRowTo() + 1, a.getColumnTo()).equalsPawn("B") && (state.getPawn(a.getRowTo() + 2, a.getColumnTo()).equalsPawn("W") || state.getPawn(a.getRowTo() + 2, a.getColumnTo()).equalsPawn("T") || state.getPawn(a.getRowTo() + 2, a.getColumnTo()).equalsPawn("K") || isCamp(a.rowTo + 2, a.columnTo))) {
            state.removePawn(a.getRowTo() + 1, a.getColumnTo());
//            this.movesWithutCapturing = -1;
//            this.loggGame.fine("Pedina nera rimossa in: " + state.getBox(a.getRowTo() + 1, a.getColumnTo()));
        }
        //controllo se ho vinto
        if (a.getRowTo() == 0 || a.getRowTo() == state.getBoard().length - 1 || a.getColumnTo() == 0 || a.getColumnTo() == state.getBoard().length - 1) {
            if (state.getPawn(a.getRowTo(), a.getColumnTo()).equalsPawn("K")) {
                state.setTurn(State.Turn.WHITEWIN);
//                this.loggGame.fine("Bianco vince con re in " + a.getTo());
            }
        }

        //controllo il pareggio
//        if (this.movesWithutCapturing >= this.movesDraw && (state.getTurn().equalsTurn("B") || state.getTurn().equalsTurn("W"))) {
//            state.setTurn(State.Turn.DRAW);
//            this.loggGame.fine("Stabilito un pareggio per troppe mosse senza mangiare");
//        }
//        this.movesWithutCapturing++;
        return state;
    }

    /**
     * This method check if a pawn is captured and if the game ends
     *
     * @param state the state of the game
     * @param a the action of the previous moved pawn
     * @return the new state of the game
     */
    private State checkCaptureBlack(State state, Action a) {
        
        int rowTo = a.getRowTo();
        int columnTo = a.getColumnTo();
        
        //Mangio pedina bianca sopra
        if( (rowTo > 1) &&
            state.getPawn(rowTo - 1, columnTo).equalsPawn("W") &&
            (
                state.getPawn(rowTo - 2, columnTo).equalsPawn("B") ||
                isCamp(rowTo - 2, columnTo) ||
                state.getPawn(rowTo - 2, columnTo).equalsPawn("T")
            )){
            state.removePawn(rowTo - 1, columnTo);
        }
        //Mangio pedina bianca sotto
        if( (rowTo < 7) &&
            state.getPawn(rowTo + 1, columnTo).equalsPawn("W") &&
            (
                state.getPawn(rowTo + 2, columnTo).equalsPawn("B") ||
                isCamp(rowTo + 2, columnTo) ||
                state.getPawn(rowTo + 2, columnTo).equalsPawn("T")
            )){
            state.removePawn(rowTo + 1, columnTo);
        }
        //Mangio pedina bianca a destra
        if( (columnTo < 7) &&
            state.getPawn(rowTo, columnTo + 1).equalsPawn("W") &&
            (
                state.getPawn(rowTo, columnTo + 2).equalsPawn("B") ||
                isCamp(rowTo, columnTo + 2) ||
                state.getPawn(rowTo, columnTo + 2).equalsPawn("T")
            )){
            state.removePawn(rowTo, columnTo + 1);
        }
        //Mangio pedina bianca a sinistra
        if( (columnTo > 1) &&
            state.getPawn(rowTo, columnTo - 1).equalsPawn("W") &&
            (
                state.getPawn(rowTo, columnTo - 2).equalsPawn("B") ||
                isCamp(rowTo, columnTo - 2) ||
                state.getPawn(rowTo, columnTo - 2).equalsPawn("T")
            )){
            state.removePawn(rowTo, columnTo - 1);
        }
        
        //Trono - Special case
        //Sopra
        if((rowTo > 1) && state.getPawn(rowTo - 1, columnTo).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo - 1, columnTo)){
                    if(state.aroundKingPieces(rowTo - 1, columnTo) == 3){
                        state.removePawn(rowTo - 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo - 2, columnTo).equalsPawn("B") || isCamp(rowTo - 2, columnTo)){
                        state.removePawn(rowTo - 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //Sotto
        if((rowTo < 7) && state.getPawn(rowTo + 1, columnTo).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo + 1, columnTo)){
                    if(state.aroundKingPieces(rowTo + 1, columnTo) == 3){
                        state.removePawn(rowTo + 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo + 2, columnTo).equalsPawn("B") || isCamp(rowTo + 2, columnTo)){
                        state.removePawn(rowTo + 1, columnTo);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //Destra
        if((columnTo < 7) && state.getPawn(rowTo, columnTo + 1).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo, columnTo + 1)){
                    if(state.aroundKingPieces(rowTo, columnTo + 1) == 3){
                        state.removePawn(rowTo, columnTo + 1);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo, columnTo + 2).equalsPawn("B") || isCamp(rowTo, columnTo + 2)){
                        state.removePawn(rowTo, columnTo + 1);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //Sinistra
        if((columnTo > 1) && state.getPawn(rowTo, columnTo - 1).equalsPawn("K") && !state.getPawn(4, 4).equalsPawn("K")){
            if(isNearThrone(state, rowTo, columnTo - 1)){
                    if(state.aroundKingPieces(rowTo, columnTo - 1) == 3){
                        state.removePawn(rowTo, columnTo - 1);
                        state.setTurn(State.Turn.BLACKWIN);
                    }
            }else{
                if(state.getPawn(rowTo, columnTo - 2).equalsPawn("B") || isCamp(rowTo, columnTo - 2)){
                        state.removePawn(rowTo, columnTo - 1);
                        state.setTurn(State.Turn.BLACKWIN);
                }
            }
        }
        
        //controllo il re completamente circondato
        if (state.getPawn(4, 4).equalsPawn(State.Pawn.KING.toString()) && state.getBoard().length == 9) {
            if (state.getPawn(3, 4).equalsPawn("B") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B")) {
                state.setTurn(State.Turn.BLACKWIN);
            }
        }
        
        //controllo regola 11
        if (state.getBoard().length == 9) {
            if (a.getColumnTo() == 4 && a.getRowTo() == 2) {
                if (state.getPawn(3, 4).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B")) {
                    state.removePawn(3, 4);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(3, 4));
                }
            }
            if (a.getColumnTo() == 4 && a.getRowTo() == 6) {
                if (state.getPawn(5, 4).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B") && state.getPawn(3, 4).equalsPawn("B")) {
                    state.removePawn(5, 4);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(5, 4));
                }
            }
            if (a.getColumnTo() == 2 && a.getRowTo() == 4) {
                if (state.getPawn(4, 3).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(3, 4).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B") && state.getPawn(4, 5).equalsPawn("B")) {
                    state.removePawn(4, 3);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(4, 3));
                }
            }
            if (a.getColumnTo() == 6 && a.getRowTo() == 4) {
                if (state.getPawn(4, 5).equalsPawn("W") && state.getPawn(4, 4).equalsPawn("K") && state.getPawn(4, 3).equalsPawn("B") && state.getPawn(5, 4).equalsPawn("B") && state.getPawn(3, 4).equalsPawn("B")) {
                    state.removePawn(4, 5);
//                    this.movesWithutCapturing = -1;
//                    this.loggGame.fine("Pedina bianca rimossa in: " + state.getBox(4, 5));
                }
            }
        }
        return state;
    }
    
    private boolean isNearThrone(State state, int x,int y){
        if(state.getPawn(x + 1, y).equalsPawn("T") || state.getPawn(x - 1, y).equalsPawn("T") || state.getPawn(x, y + 1).equalsPawn("T") || state.getPawn(x, y - 1).equalsPawn("T")){
            return true;
        }
        return false;
    }
}
