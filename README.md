# KebiTablut
## Desription
This is a Tablut player that uses a minimax algorithm implemented in Java to choose the next move.

It is also available a Prolog Tablut implementation that also uses the alpha beta pruning, but it is not capable of reaching
Java depth. This is compensated by a more specific heuristic function that makes it a player skilled almost like the Java
implementation, that is able to go deeper in the tree with the same hardware.

The Prolog library used is tuprolog. It is present a SWI-Prolog attempt, but it loops in certain situation.

We are totally aware that the project is much more refinable and that there is still room for improvements.
We are open to suggestions and even change to the code.

# How to run a player

The players can be launched with the jar file in the /dist folder with the command:

    java -jar KebiTablut.jar [color] [timeout] [ip_address]    

where:  
	**color** is the color of the player that you want to start  
	**timeout** is the amount of time in seconds that the player has to give a response  
	**ip_address** is the ip address where the Tablut server is running
	
Examples:

	java -jar KebiTablut.jar white 60 localhost
	java -jar KebiTablut.jar black 45 192.168.137.43
