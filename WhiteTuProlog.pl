%debug: initial_board(Board), alphabeta(Board, -500, 500, GoodPos, Val, white, 0).

getAction(Board,GoodPos, XFrom, YFrom, XTo, YTo, Turn) :-
	getCoordinates(Board, GoodPos, XTo, YTo, Turn), 
	getCoordinates(GoodPos,Board, XFrom, YFrom, Turn).

getCoordinates(Board,[piece(Color, Type, X, Y)|T], X, Y, Turn) :-
		Turn == Color,
		\+ member(piece(Color,Type,X,Y),Board), !.

getCoordinates(Board,[piece(Color, Type, X, Y)|T], TmpX, TmpY, Turn) :-
		getCoordinates(Board,T,TmpX,TmpY, Turn).
		
alphabeta(Pos, Alpha, Beta, GoodPos, Val, Player, Depth) :-
	Depth < 2,
	moves(Pos, Pos, PosList, Player), !,
	changeTurn(Player, NextTurn),
	boundedbest(PosList, Alpha, Beta, GoodPos, Val, NextTurn, Depth);
	staticval(Pos, Val, Depth).
	
boundedbest([Pos | PosListT], Alpha, Beta, GoodPos, GoodVal, Player, Depth) :-
	NewDepth is Depth + 1,
	alphabeta(Pos, Alpha, Beta, _, Val, Player, NewDepth),
	goodenough(PosListT, Alpha, Beta, Pos, Val, GoodPos, GoodVal, Player, Depth).
	
goodenough([], _, _, Pos, Val, Pos, Val, Player, Depth):- !.

goodenough(_, Alpha, Beta, Pos, Val, Pos, Val, Player, Depth) :-
	min_to_move(Player), Val >= Beta, !;
	max_to_move(Player), Val =< Alpha, !.
	
goodenough(PosList, Alpha, Beta, Pos, Val, GoodPos, GoodVal, Player, Depth) :-
	newbounds(Alpha, Beta, Pos, Val, NewAlpha, NewBeta, Player),
	boundedbest(PosList, NewAlpha, NewBeta, Pos1, Val1, Player, Depth),
	betterof(Pos, Val, Pos1, Val1, GoodPos, GoodVal, Player).
	
newbounds(Alpha, Beta, Pos, Val, Val, Beta, Player) :-
	min_to_move(Player), Val > Alpha, !.
	
newbounds(Alpha, Beta, Pos, Val, Alpha, Val, Player) :-
	max_to_move(Player), Val < Beta, !.
	
newbounds(Alpha, Beta, _, _, Alpha, Beta, Player).

betterof( Pos, Val, Pos1, Val1, Pos, Val, Player) :-
	min_to_move(Player), Val >= Val1, !;
	max_to_move(Player), Val =< Val1, !.
	
betterof(Pos, Val, Pos1, Val1, Pos1, Val1, Player).

changeTurn(white,black).
changeTurn(black,white).

min_to_move(black).
max_to_move(white).

/*min_to_move(Player, NextTurn) :-
	Player == NextTurn.

max_to_move(Player, NextTurn) :-
	Player \== NextTurn.*/

/*-------EURISTICA-------*/

staticval(Board, Val, Moves) :-
	(
		member(piece(white,king, KingX, KingY), Board),
		%Se la partita e vinta per i bianchi la valutazione è 100 meno il numero di mosse impiegate per arrivarci
		winningWhitePosition(KingX, KingY, Board),
		Val is 220 - Moves, !
	);
	(
	% Altrimenti dipende dal numero di pezzi sulla scacchiera e dal numero di pezzi intorno al re
	countPiece(white, Board, WhiteCount),
	countPiece(black, Board, BlackCount),

	WTempCount is (2)* WhiteCount,
	BTempCount is 16-BlackCount,
	CountVal is WTempCount + BTempCount,
	
	%distanza del re dalla vittoria, se ci puo' arrivare
	kingDistance(Board, KMoves),

	%conta dei pezzi intorno al re
	aroundKingPieces(Board, ArKingPieces),
	KPCount is (4 * ArKingPieces),
	Val is (CountVal - KPCount + (10 * (4+KMoves)))).
	
kingDistance(Board, KMoves) :-
	member(piece(white, king, X, Y), Board), !,
	horizontalMoves(piece(white, king, X, Y), Board, RightPosList, 1),
	horizontalMoves(piece(white, king, X, Y), Board, LeftPosList, -1),
	append(RightPosList,LeftPosList, HPosList),
	verticalMoves(piece(white, king, X, Y), Board, DownPosList, 1),
	verticalMoves(piece(white, king, X, Y), Board, UpPosList, -1),
	append(DownPosList, UpPosList, VPosList),
	append(HPosList, VPosList, WinPosList),
	searchWinPosition(WinPosList, KMoves).

searchWinPosition([], KMoves) :-
	KMoves is 0.

searchWinPosition([PosListH|PosListT], KMoves) :-
	member(piece(white,king, KingX, KingY), PosListH),
	winningWhitePosition(KingX, KingY, PosListH), 
	searchWinPosition(PosListT, Temp),
	KMoves is Temp+1.
	
searchWinPosition([PosListH|PosListT], KMoves) :-
	searchWinPosition(PosListT, KMoves),!.
	


aroundKingPieces(Board, ArKingPieces) :-
	member(piece(white, king, X, Y), Board), !,
	isRight(Board, X, Y, RightCount),
	isLeft(Board,X, Y, LeftCount),
	isTop(Board, X, Y, TopCount),
	isDown(Board,X, Y, BottomCount),
	HorizontalCount is RightCount + LeftCount,
	VerticalCount is TopCount + BottomCount,
	ArKingPieces is HorizontalCount + VerticalCount.

aroundKingPieces(Board, ArKingPieces) :-
	ArKingPieces is 5.

isRight(Board, X, Y, Count) :-
	PawnY is Y + 1,
	member(piece(black, pawn, X, PawnY), Board),
	Count = 1;
	Count = 0.

isLeft(Board, X, Y, Count) :-
	PawnY is Y - 1,
	member(piece(black, pawn, X, PawnY), Board),
	Count = 1;
	Count = 0.

isTop(Board, X, Y, Count) :-
	PawnX is X - 1,
	member(piece(black, pawn, PawnX, Y), Board),
	Count = 1;
	Count = 0.

isDown(Board, X, Y, Count) :-
	PawnX is X + 1,
	member(piece(black, pawn, PawnX, Y), Board),
	Count = 1;
	Count = 0.

countPiece(Color, [], Count) :-
	Count is 0, !.

countPiece(Color, [piece(C, Type, X, Y)|BoardT], Count) :-
	Color == C, !,
	countPiece(Color, BoardT, TempCount),
	Count is TempCount + 1.

countPiece(Color, [BoardH|BoardT], Count) :-
	countPiece(Color, BoardT, Count).
	
/*--------------------------------------*/
/*------------FINE EURISTICA------------*/
/*--------------------------------------*/

initial_board(Board):-
	findall(Piece, initial(Piece), Board).

isSponda(X, Y) :-
	X < 1;
	X > 9;
	Y < 1;
	Y > 9.

isCamp(X, Y) :-
	(X = 1 ; X = 9),
	(Y = 4 ; Y = 6 ; Y=5);
	(Y = 1; Y = 9),
	(X = 4 ; X = 6 ; X=5);
	X = 5, Y = 2;
	X = 5, Y = 8;
	X = 2, Y = 5;
	X = 8, Y = 5.

winningWhitePosition(KingX, KingY, Board) :-
	KingX = 1;
	KingX = 9;
	KingY = 1;
	KingY = 9;
	countPiece(black, Board, Count),
	Count == 0.

/*Itera l'operazione 'q' sulla lista*/   %DEBUG :- initial_board(Board), moves(Board, Board, PosList, white).
moves(Board, FixedBoard, PosList, Turno) :-
	member(piece(white, king, X, Y), FixedBoard),																	%fallisce se il nero ha vinto
	winningWhitePosition(X, Y, FixedBoard),
	!, fail.

moves(Board, FixedBoard, PosList, Turno) :-
	\+ member(piece(white, king, X, Y), FixedBoard), !, fail.
	
moves([], Board, PosList, Turno) :-
	append([],[],PosList), !.

moves([Head | Tail], Board, PosList,Turno) :-	
	q(Head, Board, MyPosList,Turno),
	moves(Tail, Board, NewPost,Turno),
	append(MyPosList, NewPost, PosList).
	
q(piece(Color, Type, X, Y), Board, PosList,Turno) :- %regola che blocca q se il pezzo non rispetta il turno -> vera se non tocca a te -> falsa se tocca a te
	Color \== Turno,
	append([],[],PosList), !.
	
q(piece(Color, Type, X, Y), Board, PosList,Turno) :-
	horizontalMoves(piece(Color, Type, X, Y), Board, HList1, 1),
	horizontalMoves(piece(Color, Type, X, Y), Board, HList2, -1),
	append(HList1, HList2, HPosList),
	verticalMoves(piece(Color, Type, X, Y), Board, VList1, 1),
	verticalMoves(piece(Color, Type, X, Y), Board, VList2, -1),
	append(VList1, VList2, VPosList),
	append(HPosList, VPosList, PosList).

horizontalMoves(piece(Color, Type, X, Y), Board, PosList, Passo) :- 
		YTEMP is Y + Passo,
		(
			member(piece(_, _, X, YTEMP), Board);
			isSponda(X, YTEMP);
			(	X == 5,
				YTEMP == 5
			);
			(
				\+ isCamp(X, Y),
				isCamp(X, YTEMP)
			)
		),
		append([],[],PosList),!.

horizontalMoves(piece(Color, Type, X, Y), Board, PosList, Passo):-
	YTEMP is Y + Passo,
	delete(piece(Color, Type,X,Y),Board,TempBoard),		
	append([piece(Color, Type,X,YTEMP)],TempBoard,NewBoard),
	eaten(piece(Color, Type, X, YTEMP), NewBoard, EatenBoard),
	horizontalMoves(piece(Color, Type,X,YTEMP),NewBoard,TempPosList,Passo),
	append([EatenBoard],TempPosList,PosList).

verticalMoves(piece(Color, Type, X, Y), Board, PosList, Passo) :- 
		XTEMP is X + Passo,
		(
			
			member(piece(_, _, XTEMP, Y), Board);
			isSponda(XTEMP, Y);
			(	XTEMP == 5,
				Y == 5);
			(
				\+ isCamp(X, Y),
				isCamp(XTEMP, Y)
			)
		),
		append([],[],PosList), !.

verticalMoves(piece(Color, Type, X, Y), Board, PosList, Passo):-
	XTEMP is X + Passo,
	delete(piece(Color, Type,X,Y),Board,TempBoard),		
	append([piece(Color, Type,XTEMP,Y)],TempBoard,NewBoard),
	eaten(piece(Color, Type, XTEMP, Y), NewBoard, EatenBoard),
	verticalMoves(piece(Color, Type,XTEMP,Y),NewBoard,TempPosList,Passo),
	append([EatenBoard],TempPosList,PosList).

/*----------REGOLE PER VERIFICA PEDINE MANGIATE--------------*/

eaten(piece(Color, Type, X,Y), Board, NewBoard) :-
	eatTop(piece(Color, Type, X,Y), Board, TopTemp),
	eatBottom(piece(Color, Type, X,Y), TopTemp, BottomTemp),
	eatLeft(piece(Color, Type, X,Y), BottomTemp, LeftTemp),
	eatRight(piece(Color, Type, X,Y), LeftTemp, NewBoard).

eatTop(piece(Color, Type, X,Y), Board, NewBoard) :-
	XTEMP is X - 1,
	member(piece(EatenColor, EatenType, XTEMP,Y),Board),
	EatenColor \== Color,
	XTEMP2 is X - 2,
	(
		(
			EatenType == king,
			eatableKing(XTEMP, Y, Board)
		);
		(
			EatenType == pawn,
			(member(piece(Color, _, XTEMP2, Y),Board);
			isCamp(XTEMP2, Y) )
		)
	),
	delete(piece(EatenColor, _,XTEMP,Y),Board,NewBoard).

eatTop(piece(Color, Type, X,Y), Board, NewBoard) :-
	append(Board,[],NewBoard).
	
eatBottom(piece(Color, Type, X,Y), Board, NewBoard) :-
	XTEMP is X + 1,
	member(piece(EatenColor, EatenType, XTEMP,Y),Board),
	EatenColor \== Color,
	XTEMP2 is X + 2,
	(
		(
			EatenType == king,
			eatableKing(XTEMP, Y, Board)
		);
		(
			EatenType == pawn,
			(member(piece(Color, _, XTEMP2, Y),Board);
			isCamp(XTEMP2, Y) )
		)
	),
	delete(piece(EatenColor, _,XTEMP,Y),Board,NewBoard).

eatBottom(piece(Color, Type, X,Y), Board, NewBoard) :-
	append(Board,[],NewBoard).

eatRight(piece(Color, Type, X,Y), Board, NewBoard) :-
	YTEMP is Y + 1,
	member(piece(EatenColor, EatenType, X,YTEMP),Board),
	EatenColor \== Color,
	YTEMP2 is Y + 2,
	(
		(
			EatenType == king,
			eatableKing(X, YTEMP, Board)
		);
		(
			EatenType == pawn,
			(member(piece(Color, _, X, YTEMP2),Board);
			isCamp(X, YTEMP2) )
		)
	),
	delete(piece(EatenColor, _,X,YTEMP),Board,NewBoard).

eatRight(piece(Color, Type, X,Y), Board, NewBoard) :-
	append(Board,[],NewBoard).

eatLeft(piece(Color, Type, X,Y), Board, NewBoard) :-
	YTEMP is Y - 1,
	member(piece(EatenColor, EatenType, X,YTEMP),Board),
	EatenColor \== Color,
	YTEMP2 is Y - 2,
	(
		(
			EatenType == king,
			eatableKing(X, YTEMP, Board)
		);
		(
			EatenType == pawn,
			(member(piece(Color, _, X, YTEMP2),Board);
			isCamp(X, YTEMP2) )
		)
	),
	delete(piece(EatenColor, _,X,YTEMP),Board,NewBoard).

eatLeft(piece(Color, Type, X,Y), Board, NewBoard) :-
	append(Board,[],NewBoard).

eatableKing(KingX, KingY, Board):-
	TopKingX is KingX - 1,
	BottomKingX is KingX + 1, 
	LeftKingY is KingY - 1,
	RightKingY is KingY + 1,
	(
		(	KingY = 5,
			KingX = 5,
			member(piece(black, pawn, TopKingX, KingY), Board),
			member(piece(black, pawn, BottomKingX, KingY), Board),
			member(piece(black, pawn, KingX, LeftKingY),Board),
			member(piece(black, pawn, KingX, RightKingY), Board)
		);

		(
			(	KingX =\= 5;
				KingY =\= 5	),
			( member(piece(black, pawn, TopKingX, KingY), Board);
				isCamp(TopKingX, KingY) ),
			(	member(piece(black, pawn, BottomKingX, KingY), Board);
				isCamp(BottomKingX, KingY) )
		);

		(
			(	KingX =\= 5;
				KingY =\= 5	),
			(	member(piece(black, pawn, KingX, LeftKingY), Board);
				isCamp(KingX, LeftKingY) ),
			(	member(piece(black, pawn, KingX, RightKingY), Board);
				isCamp(KingX, RightKingY)	)
		)
	).

